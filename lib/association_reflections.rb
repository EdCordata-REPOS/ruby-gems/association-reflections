# frozen_string_literal: true

module AssociationReflections
  require_relative 'association_reflections/association'
  require_relative 'association_reflections/concern'
  require_relative 'association_reflections/version'
end
